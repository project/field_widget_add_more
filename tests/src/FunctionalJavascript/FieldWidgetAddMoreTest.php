<?php

namespace Drupal\Tests\field_widget_add_more;

use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Field widget add more tests.
 *
 * @group field_widget_add_more
 *
 * @method \Drupal\FunctionalJavascriptTests\JSWebAssert assertSession($name = NULL)
 */
class FieldWidgetAddMoreTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['field_widget_add_more', 'field_test', 'entity_test'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The list of fields to test.
   *
   * @var array
   */
  protected array $fields;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $account = $this->drupalCreateUser([
      'view test entity',
      'administer entity_test content',
    ]);
    $this->assertInstanceOf(AccountInterface::class, $account);
    $this->drupalLogin($account);

    $this->fields = [
      'field_single' => 1,
      'field_multiple_limited' => random_int(2, 10),
      'field_multiple_limited_add_more' => random_int(2, 10),
      'field_unlimited' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
    ];

    foreach ($this->fields as $field_name => $cardinality) {
      $field = [
        'field_name' => $field_name,
        'entity_type' => 'entity_test',
        'bundle' => 'entity_test',
        'label' => $this->randomMachineName() . '_label',
        'description' => '[site:name]_description',
        'weight' => random_int(0, 127),
        'settings' => [
          'test_field_setting' => $this->randomMachineName(),
        ],
      ];

      FieldStorageConfig::create([
        'field_name' => $field_name,
        'entity_type' => 'entity_test',
        'type' => 'test_field',
        'cardinality' => $cardinality,
      ])->save();
      FieldConfig::create($field)->save();

      $options = match ($field_name) {
        'field_multiple_limited_add_more' => [
          'third_party_settings' => [
            'field_widget_add_more' => ['add_more' => TRUE],
          ],
        ],
        default => [],
      };

      $entity_form_display = EntityFormDisplay::load($field['entity_type'] . '.' . $field['bundle'] . '.default');
      $this->assertInstanceOf(EntityFormDisplayInterface::class, $entity_form_display);
      $entity_form_display->setComponent($field['field_name'], $options)->save();
    }
  }

  /**
   * Test field widget add more.
   */
  public function testFieldWidgetAddMore(): void {
    $this->drupalGet(Url::fromRoute('entity.entity_test.add_form'));
    $this->assertSingleField();
    $this->assertMultipleFields();
    $this->assertMultipleFieldAddMore();
    $assert_session = $this->assertSession();

    $assert_session->buttonExists('field_unlimited_add_more');
  }

  /**
   * Assert that the single field is not impacted.
   */
  protected function assertSingleField(): void {
    $this->assertSession()->buttonNotExists('field_single_add_more');
  }

  /**
   * Assert that the multiple fields without add more option are not impacted.
   */
  protected function assertMultipleFields(): void {
    $assert_session = $this->assertSession();
    $assert_session->buttonNotExists('field_multiple_limited_add_more');
    $assert_session->elementsCount('css', '#field-multiple-limited-values tbody tr', $this->fields['field_multiple_limited']);
  }

  /**
   * Assert multiple fields with add more option.
   */
  protected function assertMultipleFieldAddMore(): void {
    $assert_session = $this->assertSession();
    $add_more_button = $assert_session->buttonExists('field_multiple_limited_add_more_add_more');

    $count = 1;
    while ($count < $this->fields['field_multiple_limited_add_more']) {
      $add_more_button->click();
      $assert_session->assertWaitOnAjaxRequest();
      $assert_session->fieldExists('field_multiple_limited_add_more[' . $count . '][value]');
      $count++;
    }

    $assert_session->buttonNotExists('field_multiple_limited_add_more_add_more');

    $assert_session->buttonExists('field_multiple_limited_add_more_' . ($count - 1) . '_remove_button')->click();
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->buttonExists('field_multiple_limited_add_more_add_more');
  }

}
