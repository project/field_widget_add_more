<?php

namespace Drupal\field_widget_add_more\Hook;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\FocusFirstCommand;
use Drupal\Core\Ajax\InsertCommand;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Hook implementations for field_widget_add_more.
 */
final class FieldWidgetHooks {

  use StringTranslationTrait;

  public function __construct(TranslationInterface $stringTranslation) {
    $this->setStringTranslation($stringTranslation);
  }

  /**
   * Implements hook_field_widget_third_party_settings_form().
   */
  #[Hook('field_widget_third_party_settings_form')]
  public function thirdPartySettingsForm(WidgetInterface $plugin, FieldDefinitionInterface $field_definition): array {
    $elements = [];

    $cardinality = $field_definition->getFieldStorageDefinition()->getCardinality();
    if ($cardinality === FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED) {
      return $elements;
    }
    if ($cardinality === 1) {
      return $elements;
    }

    $elements['add_more'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show add more button'),
      '#default_value' => $plugin->getThirdPartySetting('field_widget_add_more', 'add_more'),
    ];

    return $elements;
  }

  /**
   * Implements hook_field_widget_complete_form_alter().
   *
   * @phpstan-param array{
   *   form: array,
   *   widget: \Drupal\Core\Field\WidgetInterface,
   *   items: \Drupal\Core\Field\FieldItemListInterface<\Drupal\Core\Field\FieldItemInterface>
   * } $context
   */
  #[Hook('field_widget_complete_form_alter')]
  public function completeFormAlter(array &$field_widget_complete_form, FormStateInterface $form_state, array $context): void {
    if ($form_state->isProgrammed()) {
      return;
    }

    ['form' => $form, 'widget' => $widget, 'items' => $items] = $context;
    $setting = $widget->getThirdPartySetting('field_widget_add_more', 'add_more');
    if (!$setting) {
      return;
    }

    $parents = $form['#parents'];
    $field_name = $items->getFieldDefinition()->getName();
    $cardinality = $items->getFieldDefinition()->getFieldStorageDefinition()->getCardinality();

    $id_prefix = implode('-', array_merge($parents, [$field_name]));
    $wrapper_id = Html::getUniqueId($id_prefix . '-add-more-wrapper');

    $field_state = WidgetBase::getWidgetState($parents, $field_name, $form_state);
    if ($field_state['items_count'] === 0) {
      $field_state['items_count'] = 1;
      WidgetBase::setWidgetState($parents, $field_name, $form_state, $field_state);
    }
    $max = min($field_state['items_count'], $cardinality);

    foreach (Element::children($field_widget_complete_form['widget']) as $delta) {
      if ($delta >= $max) {
        unset($field_widget_complete_form['widget'][$delta]);
        continue;
      }

      $remove_button = [
        '#delta' => $delta,
        '#name' => str_replace('-', '_', $id_prefix) . "_{$delta}_remove_button",
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#validate' => [],
        '#submit' => [[WidgetBase::class, 'deleteSubmit']],
        '#limit_validation_errors' => [],
        '#ajax' => [
          'callback' => [WidgetBase::class, 'deleteAjax'],
          'wrapper' => $wrapper_id,
          'effect' => 'fade',
        ],
      ];
      $field_widget_complete_form['widget'][$delta]['_actions'] = [
        'delete' => $remove_button,
        '#weight' => 101,
      ];
    }

    $field_widget_complete_form['widget']['#prefix'] = '<div id="' . $wrapper_id . '">';
    $field_widget_complete_form['widget']['#suffix'] = '</div>';
    $field_widget_complete_form['widget']['add_more'] = [
      '#type' => 'submit',
      '#name' => str_replace('-', '_', $id_prefix) . '_add_more',
      '#value' => $this->t('Add another item'),
      '#attributes' => ['class' => ['field-add-more-submit']],
      '#limit_validation_errors' => [],
      '#submit' => [[WidgetBase::class, 'addMoreSubmit']],
      '#ajax' => [
        'callback' => [self::class, 'addMoreAjaxCallback'],
        'wrapper' => $wrapper_id,
        'effect' => 'fade',
      ],
      '#access' => $cardinality === FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED || $max < $cardinality,
    ];
  }

  /**
   * Ajax callback for the "Add another item" button.
   *
   * This returns the new page content to replace the page content made obsolete
   * by the form submission.
   *
   * @see \Drupal\Core\Field\WidgetBase::addMoreAjax()
   */
  public static function addMoreAjaxCallback(array $form, FormStateInterface $form_state): ?AjaxResponse {
    $button = $form_state->getTriggeringElement();
    if (!$button) {
      return NULL;
    }

    // Go one level up in the form, to the widgets container.
    $element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -1));
    if (!is_array($element)) {
      return NULL;
    }

    // Turn render array into response with AJAX commands.
    $response = new AjaxResponse();
    $response->addCommand(new InsertCommand(NULL, $element));

    $field_state = WidgetBase::getWidgetState($form['#parents'], $element['#field_name'], $form_state);

    // Add a DIV around the delta receiving the Ajax effect.
    $delta = $field_state['items_count'] - 1;
    // Construct an attribute to add to div for use as selector to set the focus
    // on.
    $button_parent = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -1));
    if (is_array($button_parent)) {
      $focus_attribute = 'data-drupal-selector="field-' . $button_parent['#field_name'] . '-more-focus-target"';
      $element[$delta]['#prefix'] = '<div class="ajax-new-content" ' . $focus_attribute . '>' . ($element[$delta]['#prefix'] ?? '');
      $element[$delta]['#suffix'] = ($element[$delta]['#suffix'] ?? '') . '</div>';

      // Add command to set the focus on first focusable element within the div.
      $response->addCommand(new FocusFirstCommand("[$focus_attribute]"));
    }
    return $response;
  }

}
