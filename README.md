## INTRODUCTION

The Field Widget Add More module adds an option to always show an add more
button to the field widget for fields with limited cardinality.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION

When configuring form display for a field, an additional option is added to always
show add more button if the field is multiple with limited cardinality. When
this option is checked, the field widget will only show necessary items with
add more button, similar to multiple fields with unlimited cardinality.

The add more button won't be displayed once the items are at the maximum number.

## MAINTAINERS

Current maintainers for Drupal 10:

- Luhur Abdi Rizal (el7cosmos) - https://www.drupal.org/u/el7cosmos
